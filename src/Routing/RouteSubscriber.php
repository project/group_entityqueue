<?php

namespace Drupal\group_entityqueue\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\group_entityqueue\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Alter entity queues routes defined on entityqueue.routing.yml file.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   Routes collection.
   */
  public function alterRoutes(RouteCollection $collection) {
    $routes = $collection->all();
    foreach ($routes as $route_name => $route) {
      switch ($route_name) {
        case 'entity.entity_queue.subqueue_list':
          $route->setRequirements(['_custom_access' => '\Drupal\group_entityqueue\Access\GroupEntityqueueAccess::entityqueueViewAccess']);
          break;

        case 'entity.entity_queue.edit_form':
          $route->setRequirements(['_custom_access' => '\Drupal\group_entityqueue\Access\GroupEntityqueueAccess::entityqueueEditAccess']);
          break;

        case 'entity.entity_queue.delete_form':
          $route->setRequirements(['_custom_access' => '\Drupal\group_entityqueue\Access\GroupEntityqueueAccess::entityqueueDeleteAccess']);
          break;

        case 'entity.entity_subqueue.add_form':
        case 'entity.entity_subqueue.add_item':
        case 'entity.entity_subqueue.remove_item':
          $route->setRequirements(['_custom_access' => '\Drupal\group_entityqueue\Access\GroupEntityqueueAccess::entityqueueManipulateAccess']);
          break;
      }
    }
  }

}
