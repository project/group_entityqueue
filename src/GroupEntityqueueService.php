<?php

namespace Drupal\group_entityqueue;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\GroupMembershipLoader;
use Drupal\entityqueue\EntityQueueInterface;

/**
 * Checks access for displaying entity queues and sub queues pages.
 */
class GroupEntityqueueService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user's account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoader
   */
  protected $membershipLoader;

  /**
   * An array containing the entity queues for a user.
   *
   * @var array
   */
  protected $userEntityqueues = [];

  /**
   * An array containing the entity queues for a user and group.
   *
   * @var array
   */
  protected $userGroupEntityqueues = [];

  /**
   * Static cache of all group entityqueue objects keyed by group ID.
   *
   * @var \Drupal\entityqueue\EntityQueueInterface[][]
   */
  protected $groupEntityqueues = [];

  /**
   * An array containing the entity queue access results.
   *
   * @var array
   */
  protected $entityqueueAccess = [];

  /**
   * Constructs a new GroupTypeController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\group\GroupMembershipLoader $membership_loader
   *   The group membership loader.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, GroupMembershipLoader $membership_loader) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->membershipLoader = $membership_loader;
  }

  /**
   * Custom access check based on operation and entityqueue object.
   *
   * @param string $op
   *   The operation being executed.
   * @param \Drupal\entityqueue\EntityQueueInterface $entityqueue
   *   The entiyqueue object which the operation is being executed on.
   * @param Drupal\Core\Session\AccountInterface|null $account
   *   The current logged in user account.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   The access result based on group permissions.
   */
  public function entityqueueAccess($op, EntityQueueInterface $entityqueue, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }

    if (isset($this->entityqueueAccess[$op][$account->id()][$entityqueue->id()])) {
      return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()];
    }

    if ($account->hasPermission('administer entityqueue') ||
      $account->hasPermission('manipulate all entityqueues') ||
      $account->hasPermission('manipulate entityqueues')) {
      return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()] = AccessResult::allowed();
    }

    $plugin_id = 'group_entityqueue';
    $group_content_types = $this->entityTypeManager->getStorage('group_content_type')
      ->loadByContentPluginId($plugin_id);
    if (empty($group_content_types)) {
      return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()] = AccessResult::neutral();
    }

    // Load all the group content for this entity queue.
    $group_contents = $this->entityTypeManager->getStorage('group_content')
      ->loadByProperties([
        'type' => array_keys($group_content_types),
        'entity_id_str' => $entityqueue->id(),
      ]);

    // If the entity queue does not belong to any group, we have nothing to say.
    if (empty($group_contents)) {
      return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()] = AccessResult::neutral();
    }

    /** @var \Drupal\group\Entity\GroupInterface[] $groups */
    $groups = [];
    foreach ($group_contents as $group_content) {
      /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
      $group = $group_content->getGroup();
      $groups[$group->id()] = $group;
    }

    // From this point on you need group to allow you to perform the requested
    // operation. If you are not granted access for a group, you should be
    // denied access instead.
    foreach ($groups as $group) {
      if ($group->hasPermission("$op $plugin_id entity", $account)) {
        return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()] = AccessResult::allowed();
      }
    }

    return $this->entityqueueAccess[$op][$account->id()][$entityqueue->id()] = AccessResult::neutral();
  }

  /**
   * Get group entityqueues for the logged in user.
   *
   * @param string $op
   *   The opration being executed.
   * @param Drupal\Core\Session\AccountInterface|null $account
   *   The current logged in user account.
   *
   * @return mixed
   *   All group entityqueues for the logged in user.
   */
  public function loadUserGroupEntityqueues($op, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }

    if (isset($this->userEntityqueues[$op][$account->id()])) {
      return $this->userEntityqueues[$op][$account->id()];
    }

    $group_memberships = $this->membershipLoader->loadByUser($account);
    $this->userEntityqueues[$op][$account->id()] = [];
    foreach ($group_memberships as $group_membership) {
      $this->userEntityqueues[$op][$account->id()] += $this->loadUserGroupEntityqueuesByGroup($op, $group_membership->getGroupContent()->gid->target_id, $account);
    }

    return $this->userEntityqueues[$op][$account->id()];
  }

  /**
   * Get group entity queues for a user on a specific group.
   *
   * @param string $op
   *   The operation being executed.
   * @param int $group_id
   *   The group id.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The current logged in user account.
   *
   * @return array|EntityQueueInterface|EntityQueueInterface[]
   *   The group entityqueues for a user on a specific group.
   */
  public function loadUserGroupEntityqueuesByGroup($op, $group_id, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }

    if (isset($this->userGroupEntityqueues[$op][$account->id()][$group_id])) {
      return $this->userGroupEntityqueues[$op][$account->id()][$group_id];
    }

    $group_entityqueues = $this->getGroupEntityqueues();
    $group_entityqueue_for_group = (!empty($group_entityqueues[$group_id])) ? $group_entityqueues[$group_id] : [];

    return $this->userGroupEntityqueues[$op][$account->id()][$group_id] = $group_entityqueue_for_group;
  }

  /**
   * Get all group entityqueue objects.
   *
   * We create a static cache of group entityqueues since loading them
   * individually has a big impact on performance.
   *
   * @return \Drupal\entityqueue\EntityQueueInterface[][]
   *   A nested array containing group entityqueue objects keyed by group ID.
   */
  public function getGroupEntityqueues() {
    if (!$this->groupEntityqueues) {
      $plugin_id = 'group_entityqueue';

      $entityqueues = $this->entityTypeManager->getStorage('entity_queue')
        ->loadMultiple();

      $group_content_types = $this->entityTypeManager->getStorage('group_content_type')
        ->loadByContentPluginId($plugin_id);
      if (!empty($group_content_types)) {
        $group_contents = $this->entityTypeManager->getStorage('group_content')
          ->loadByProperties(['type' => array_keys($group_content_types)]);

        foreach ($group_contents as $group_content) {
          /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
          $this->groupEntityqueues[$group_content->getGroup()->id()][$group_content->getEntity()->id()] = $entityqueues[$group_content->getEntity()->id()];
        }
      }
    }

    return $this->groupEntityqueues;
  }

}
