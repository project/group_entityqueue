<?php

namespace Drupal\group_entityqueue\Controller;

use Drupal\group\Entity\Controller\GroupContentController;
use Drupal\group\Entity\GroupInterface;

/**
 * Controller for 'Entiti queues' tab/route.
 */
class GroupEntityqueueController extends GroupContentController {

  /**
   * Create a list of Entity queues in the group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The current group.
   *
   * @return mixed
   *   Renderable list of entity queues that belongs to the group.
   */
  public function groupContentOverview(GroupInterface $group) {
    $class = '\Drupal\group_entityqueue\GroupEntityqueueContentListBuilder';
    $definition = $this->entityTypeManager()->getDefinition('group_content');
    return $this->entityTypeManager()->createHandlerInstance($class, $definition)->render();
  }

  /**
   * Title for the Entity queues list overview.
   *
   * @param Drupal\group\Entity\GroupInterface $group
   *   The current group.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title for Entity queues group content overview list.
   */
  public function groupContentOverviewTitle(GroupInterface $group) {
    return $this->t("%label entity queues", ['%label' => $group->label()]);
  }

}
