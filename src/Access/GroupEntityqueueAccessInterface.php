<?php

namespace Drupal\group_entityqueue\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatch;

/**
 * Provides an interface defining custom access for Entity queue pages.
 */
interface GroupEntityqueueAccessInterface {

  /**
   * A custom access check for entity queue subqueues list page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The route match object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function entityqueueViewAccess(AccountInterface $account, RouteMatch $routeMatch);

  /**
   * A custom access check for entity queue edit page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The route match object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function entityqueueEditAccess(AccountInterface $account, RouteMatch $routeMatch);

  /**
   * A custom access check for entity queue delete page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The route match object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function entityqueueDeleteAccess(AccountInterface $account, RouteMatch $routeMatch);

  /**
   * A custom access check for add/delete/edit sub queues.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The route match object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function entityqueueManipulateAccess(AccountInterface $account, RouteMatch $routeMatch);

}
