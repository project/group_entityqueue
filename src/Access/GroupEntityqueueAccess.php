<?php

namespace Drupal\group_entityqueue\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatch;

/**
 * Controller for 'Entitiqueues' tab/route.
 */
class GroupEntityqueueAccess {

  /**
   * {@inheritdoc}
   */
  public function entityqueueViewAccess(AccountInterface $account, RouteMatch $routeMatch) {
    $entity_queue = $routeMatch->getParameter('entity_queue');
    return \Drupal::service('group_entityqueue.entityqueue')->entityqueueAccess('view', $entity_queue, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function entityqueueCreateAccess(AccountInterface $account, RouteMatch $routeMatch) {
    $entity_queue = $routeMatch->getParameter('entity_queue');
    return \Drupal::service('group_entityqueue.entityqueue')->entityqueueAccess('create', $entity_queue, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function entityqueueEditAccess(AccountInterface $account, RouteMatch $routeMatch) {
    $entity_queue = $routeMatch->getParameter('entity_queue');
    return \Drupal::service('group_entityqueue.entityqueue')->entityqueueAccess('update', $entity_queue, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function entityqueueDeleteAccess(AccountInterface $account, RouteMatch $routeMatch) {
    $entity_queue = $routeMatch->getParameter('entity_queue');
    return \Drupal::service('group_entityqueue.entityqueue')->entityqueueAccess('delete', $entity_queue, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function entityqueueManipulateAccess(AccountInterface $account, RouteMatch $routeMatch) {
    $entity_queue = $routeMatch->getParameter('entity_queue');
    return \Drupal::service('group_entityqueue.entityqueue')->entityqueueAccess('manipulate', $entity_queue, $account);
  }

}
